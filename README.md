<p align="center">
  <h2 align="center">Angular Full Stack Multichain Explorer</h2>
  <p align="center">The frontend is generated with <a href="https://github.com/angular/angular-cli">Angular CLI</a>. The backend is made from scratch. Whole stack in <a href="https://www.typescriptlang.org">TypeScript</a>.</p>
  <p align="center">
  </p>
</p>

## Prerequisite

* [NodeJS](https://nodejs.org): runtime environment
* [Multichain](https://www.multichain.com): blockchain database
* [Slim API](https://github.com/me-io/slim-api): backend framework
* [Angular CLI](https://cli.angular.io): frontend scaffolding

## Uses

* [Angular 7+](https://angular.io): frontend framework
* [Bootstrap](http://www.getbootstrap.com): layout and styles
* [Font Awesome](http://fontawesome.io): icons

## Prerequisites

1. Install [Node.js](https://nodejs.org) and [Multichain](https://www.multichain.com)
2. Install Angular CLI by running the following command:
  ```bash
  npm i -g @angular/cli
  ```
3. From project root folder install all the dependencies by running the following command inside your terminal:
  ```bash
  npm install
  ```

## Run the app

### Development mode

By running the following command a window will automatically open at [localhost:4200](http://localhost:4200). Angular and Express files are being watched. Any change automatically creates a new bundle, restart Express server and reload your browser.

```bash
npm start
or
npm run dev
```

### Production mode

To run the project with a production bundle and AOT compilation listening at [localhost:3000](http://localhost:3000) run the following command:

```bash
npm run prod
```

## Deployment example

1. Download this repo and copy all files into `my-project` folder
2. Now Edit `.gitignore` and remove line with `/dist` text.
3. Edit `.env` and replace the URIs and settings.
4. Install the dependencies by running the following command:
  ```bash
  npm install
  ```
5. Now build your app by running one of the following command:
  ```bash
  ng build -prod 
  >> or 
  ng build -aot -prod
  ```
6. Run the following command
  ```bash
  tsc -p server
  ```


## Running tests

Run the following command inside your terminal to execute the unit tests via [Karma](https://karma-runner.github.io/).

```bash
npm run test
```

## Contributors

A huge thanks to all of our contributors:
Geovanni

## License

The code is available under the [MIT license](LICENSE.md).
